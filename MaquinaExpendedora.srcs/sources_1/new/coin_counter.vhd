library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity count is
	port (
    	clk       : in  std_logic;
        reset     : in  std_logic;
        coins	  : in std_logic_vector(3 downto 0); --[100,50,20,10]
		enable    : in std_logic;
		enough	  : out std_logic;
        error	  : out std_logic
	);
end entity;
    
architecture behavioral of count is

	signal accum_i  : std_logic_vector(7 downto 0) := "00000000";
    constant price  : std_logic_vector(7 downto 0)  := X"64";
    signal enough_i : std_logic := '0';
    signal error_i  : std_logic:= '0';
    
begin

	comparator: process (accum_i)
	begin
        --Valores por defecto para evitar creaci�n de latches
	    enough_i <= '0';
	    error_i <= '0';
	    
		if accum_i = price then
       	   error_i  <= '0';
		   enough_i <= '1';
           
		elsif accum_i < price then
       	   error_i  <= '0';
		   enough_i <= '0';
           
        elsif accum_i > price then
       	   error_i    <= '1';
		   enough_i <= '0';
           
		end if;
	end process;

	counter: process (clk, reset, coins, enough_i)
	begin
		if rising_edge(clk) then
        	if reset = '1' or enough_i = '1' or error_i = '1' then
            	accum_i <= "00000000";
			elsif coins(0) = '1' and enable = '1'then
				accum_i <= accum_i + 10;
			elsif coins(1) = '1' and enable = '1' then
				accum_i <= accum_i + 20;
			elsif coins(2) = '1' and enable = '1' then
				accum_i <= accum_i + 50;
            elsif coins(3) = '1' and enable = '1' then
				accum_i <= accum_i + 100;
            --else
            --accum_i<= "00000000";
			end if;
		end if;
	end process;
	
    enough <= enough_i;
    error  <= error_i;
    
end behavioral;