--TIMER: temporizador para estado de espera de la m�quina de estados
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity timer is
  generic(
      clockFrequency : integer := 1e3;   -- Frec reloj 1kHz
      delay			 : integer := 3      -- Espera 3 segundos      
  );
  port(
      clk   : in std_logic;   -- Clock input
      reset : in std_logic;   -- Reset
      load	: in std_logic;   -- Entrada indicadora de carga
      zero	: out std_logic	  -- Salida indicadora de fin de la espera
  );
end entity;
 
architecture behavioral of timer is
 
    -- Se�al para contar hacia atr�s en cada flanco positivo de reloj
    signal ticks : integer; -- se cargar� cuando load se active
 
begin
    zero <= '1' when ticks = 0 else '0';
    
    process(clk, reset, load) is
    begin
    	-- si RESET a 1
    	if reset = '1' then
        	ticks   <=  0;
            
    	-- Flanco positivo de reloj
        elsif rising_edge(clk) then
 
            if load = '1' then
            	ticks   <= delay * clockFrequency;
                
            -- Se�al de carga desactivada
            elsif ticks /= 0 then
                ticks <= ticks - 1;
 			end if;
            
        end if;
        
    end process;
 
end architecture;

