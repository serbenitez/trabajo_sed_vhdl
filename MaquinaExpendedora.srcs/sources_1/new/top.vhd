LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

ENTITY top IS
    GENERIC (
        CLKIN_FREQ  : positive := 100e6;    -- Frecuencia reloj placa 100MHz
        CLKOUT_FREQ : positive := 1e3;      -- Frecuencia salida prescaler 1Hz
        DLY         : integer  := 3         -- Tiempo de espera fsm 3s
    );
    PORT ( 
        RESET_N     : IN std_logic;
        CLK         : IN std_logic;
        COINS       : IN std_logic_vector(3 downto 0);
        PRODUCT     : OUT std_logic;
        ERROR       : OUT std_logic;
        LIGHT       : OUT std_logic_vector(4 downto 0)
    );
END top;

ARCHITECTURE dataflow OF top IS
    
    --Se�ales internas
    signal SYNC     : std_logic_vector(3 downto 0);
    signal DBNCD    : std_logic_vector(3 downto 0);
    signal ERROR_I  : std_logic;
    signal ENOUGH   : std_logic;
    signal CLK_P    : std_logic;
    signal LOAD     : std_logic;
    signal ZERO     : std_logic;
    signal RESET    : std_logic;
    signal ENABLE   : std_logic;

    component prescaler is
        generic(
          freq_in  : positive := CLKIN_FREQ;
          freq_out : positive := CLKOUT_FREQ
        );
        port (
          clk_in  : in  std_logic;
          clk_out : out std_logic
        );
    end component;
    
    component SYNCHRNZR is
        port (
            CLK      : in std_logic;
            ASYNC_IN : in std_logic;
            SYNC_OUT : out std_logic
        );
    end component;

    component debouncer is
      Port (
        CLK : in std_logic;
        SYNC_IN : in std_logic;
        SYNCED_OUT : out std_logic
      );
    end component;

    component count is
        port (
            clk       : in  std_logic;
            reset     : in  std_logic;
            coins	  : in  std_logic_vector(3 downto 0); --[100,50,20,10]
            enable    : in std_logic;
            enough	  : out std_logic;
            error	  : out std_logic
        );
    end component;

    component timer is
      generic(
          clockFrequency : integer := CLKOUT_FREQ;
          delay			 : integer := DLY   
      );
      port(
          clk   : in std_logic;   -- Clock input
          reset : in std_logic;   -- Reset
          load	: in std_logic;   -- Entrada indicadora de carga
          zero	: out std_logic	  -- Salida indicadora de fin de la espera
      );
    end component;

    component fsm is
      port (
        RESET     : in  std_logic;
        CLK       : in  std_logic;
        ENOUGH    : in  std_logic;
        ERROR     : in  std_logic;
        ZERO      : in  std_logic;
        LOAD      : out std_logic;
        PRODUCT   : out std_logic;
        ERROR_OUT : out std_logic;
        LIGHT	  : out std_logic_vector(4 downto 0);
        ENABLE    : out std_logic
      );
    end component;
   
BEGIN
    RESET <= not RESET_N;
    
    inst_prescaler: prescaler
        PORT MAP(
            clk_in  => clk,
            clk_out => clk_p
        );
    
    -- Sincronizadores y debouncers
    synchronizers: for i in coins'range generate
        inst_synchronizer_i: SYNCHRNZR
            port map (
                clk      => clk_p,
                async_in => coins(i),
                sync_out => sync(i)
            );
        inst_debouncer_i: debouncer
            port map (
                clk         => clk_p,
                sync_in     => sync(i),
                synced_out  => dbncd(i)
            );
    end generate;
    
    inst_coin_counter: count
        port map (
            clk     => clk_p,
            reset   => reset,
            coins   => dbncd,
            enough  => enough,
            error   => error_i,
            enable  => enable
        );
    
    inst_timer: timer
        port map (
            clk     => clk_p,
            reset   => reset,
            load    => load,
            zero    => zero
        );
    
    inst_fsm: fsm
        port map (
            reset       => reset,
            clk         => clk_p,
            enough      => enough,
            error       => error_i,
            zero        => zero,
            load        => load,
            product     => product,
            error_out   => error,
            light       => light,
            enable      => enable
        );

end dataflow;