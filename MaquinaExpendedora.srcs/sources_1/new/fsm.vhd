--FSM design: Finite State Machine. M�quina de estados de Moore

library IEEE;
use IEEE.std_logic_1164.all;

entity fsm is
  port (
    RESET     : in  std_logic;
    CLK       : in  std_logic;
    ENOUGH    : in  std_logic;
    ERROR     : in  std_logic;
    ZERO      : in  std_logic;
    LOAD      : out std_logic;
    PRODUCT   : out std_logic;
    ERROR_OUT : out std_logic;
    LIGHT	  : out std_logic_vector(4 downto 0);
    ENABLE    : out std_logic
  );
end fsm;

architecture behavioral of fsm is

  type STATES is (S0, S1, S2, S3, S4);
  signal current_state: STATES;
  signal next_state   : STATES;
  
begin

  state_register: process (RESET, CLK)
  begin
    if RESET = '1' then
      current_state <= S0;
    elsif CLK'event and CLK = '1' then
      current_state <= next_state;    
    end if;
  end process;
  
  nextstate_decod: process (ENOUGH, ERROR, current_state, ZERO)
  begin
    next_state <= current_state;
    case current_state is
      when S0 =>
        if ENOUGH = '1' then      
          next_state <= S1;
        elsif ERROR = '1' then      
          next_state <= S2;
        end if;
      when S1 =>
        next_state <= S3;
      when S2 =>
        next_state <= S4;
      when S3 =>
        if ZERO = '1' then
          next_state <= S0;
        end if;
      when S4 =>
        if ZERO = '1' then
          next_state <= S0;
        end if;
      when others =>
        next_state <= S0;
    end case;
  end process;

  output_decod: process (current_state)
  begin
    case current_state is
      when S0 =>
        PRODUCT   <= '0';
        LIGHT     <= "00001";
        ERROR_OUT <= '0';
        LOAD      <= '0';
        ENABLE    <= '1';
      when S1 =>
        PRODUCT <= '1';
        LIGHT <= "00010";
        ERROR_OUT<= '0';
        LOAD      <= '1';
        ENABLE    <= '0';
      when S2 => 
        PRODUCT   <= '0';
        LIGHT     <= "00100";
        ERROR_OUT <= '1';
        LOAD      <= '1';
        ENABLE    <= '0';
      when S3 =>
        PRODUCT   <= '1';
        LIGHT     <= "01000";
        ERROR_OUT <= '0';
        LOAD      <= '0';
        ENABLE    <= '0';
      when S4 =>
        PRODUCT   <= '0';
        LIGHT     <= "10000";
        ERROR_OUT <= '1';
        LOAD      <= '0';
        ENABLE    <= '0';
      when others => 
        PRODUCT <= '0';
        LIGHT     <= "00000";
        ERROR_OUT <= '1';
        LOAD      <= '0';
        ENABLE    <= '0';
    end case;
  end process;
  
end behavioral;