library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity prescaler is
  generic(
    freq_in  : positive := 100e6;
    freq_out : positive := 1e3
  );
  port (
    clk_in  : in  std_logic;
    clk_out : out std_logic
    );
end prescaler;

architecture Behavioral of prescaler is

  signal cnt  : unsigned(15 downto 0) := (others => '0');
  signal clk_out_i : std_logic := '0';
  
begin

  gen_clk : process (clk_in)
  begin
    if rising_edge(clk_in) then   -- rising clock edge
-- (100Mhz/1Khz)/2= 100e6/1e3 = 100e3/2 = 50e3 en hexadecimal C350
      if cnt = ((freq_in / freq_out) / 2) then
        cnt   <= (others => '0');
        clk_out_i   <= not clk_out_i;
      else
        cnt <= cnt + 1;
      end if;
    end if;
  end process gen_clk;

  clk_out <= clk_out_i;

end Behavioral;
