--FSM testbench

library IEEE;
use IEEE.std_logic_1164.all;
library IEEE;
use IEEE.std_logic_1164.all;

entity fsm_tb is
end fsm_tb;

architecture behavior of fsm_tb is 

  -- Component Declaration for the Unit Under Test (UUT)
  component fsm
    port (
      RESET     : in  std_logic;
      CLK       : in  std_logic;
      ENOUGH    : in  std_logic;
      ERROR     : in  std_logic;
      ZERO      : in  std_logic;
      LOAD      : out std_logic;
      PRODUCT   : out std_logic;
      ERROR_OUT : out std_logic;
      LIGHT	    : out std_logic_vector(4 downto 0);
      ENABLE    : out std_logic
  );
  end component;

  --Inputs
  signal reset     : std_logic;
  signal clk       : std_logic;
  signal ENOUGH    : std_logic;
  signal ERROR     : std_logic;
  signal ZERO      : std_logic;

  --Outputs
  signal PRODUCT   : std_logic;
  signal LOAD      : std_logic;
  signal LIGHT	   : std_logic_vector(4 downto 0);
  signal ENABLE    : std_logic;

  -- Clock period definitions
  constant clk_period: time := 10 ns;

begin
  -- Instantiate the Unit Under Test (UUT)
  uut: fsm
    port map (
      RESET      => reset,
      CLK        => clk,
      ENOUGH     => ENOUGH,
      ZERO		 => ZERO,
      ERROR      => ERROR,
      PRODUCT    => PRODUCT,
      LOAD		 => LOAD,
      LIGHT		 => LIGHT,
      ENABLE     => ENABLE
    );

  -- Clock process definitions
  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

  reset <= '0', '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;

  -- Stimulus process
  stim_proc: process
  begin
  	-- s0
  	ERROR <= '0';
    ENOUGH <= '0';
    ZERO <= '0';

    wait for 2.25 * clk_period;

    ENOUGH <= '1';
    --s1 - s3
    wait for 2 * clk_period;
    ENOUGH <= '0';
    ZERO <= '1';
    --S0
    wait for 1 * clk_period;
    ZERO <= '0';
    ERROR <= '1';
    --S2 - S4
    wait for 2 * clk_period;
    ERROR <= '0';
    ZERO <= '1';
    --S0
    wait for 1 * clk_period;
    ZERO <= '0';
    
    wait for 2 * clk_period;

    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
      
  end process;
  
end;