library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity top_tb is
end entity;

architecture test of top_tb is

  -- Inputs
  signal clk, reset_n : std_logic;
  signal coins	    : std_logic_vector(3 downto 0);
  
  -- Outputs
  signal product : std_logic;
  signal error	 : std_logic;
  signal light   : std_logic_vector(4 downto 0);

    component top is
        generic (
            CLKIN_FREQ  : positive   -- Frecuencia reloj placa 100MHz
        );
        port ( 
            RESET_N      : IN std_logic;
            CLK         : IN std_logic;
            COINS       : IN std_logic_vector(3 downto 0);
            PRODUCT     : OUT std_logic;
            ERROR       : OUT std_logic;
            LIGHT       : OUT std_logic_vector(4 downto 0)
        );
    end component;

  constant CLK_IN_PERIOD  : time := 100 us; -- Periodo reloj de la placa
  
begin
  -- Unit Under Test
  uut: top
    generic map(
        CLKIN_FREQ => 10_000
    )
    port map (
	  clk       => clk,
      reset_n   => reset_n,
      coins 	=> coins,
	  product   => product,
      error     => error,
      light     => light
    );

  clkgen: process
  begin
    clk <= '1';
    wait for 0.5 * CLK_IN_PERIOD;
    clk <= '0';
    wait for 0.5 * CLK_IN_PERIOD;
  end process;
  
  tester: process
  begin
    reset_n <= '0';
    wait for 1 ms;
    reset_n <= '1';
    coins <= "0000";
    -- Metemos 1� justo
    wait for 10 ms;
    coins(0) <= '1'; --10c
    wait for 10 ms;
    coins(0)   <= '0';
    coins(1) <= '1'; --20c
    wait for 10 ms;
    coins(1) <= '0';
    wait for 10 ms;
    coins(1) <= '1'; --20c
    wait for 10 ms;
    coins(1) <= '0';
    coins(2) <= '1'; --50c
    wait for 10 ms;
    coins(2) <= '0';
    --Hay 1� -> producto se enciende 3s
    wait for 1000 ms;
    --Metemos una moneda de 1� con el enable apagado (S3) para comprobar que no la cuenta el acumulador
    coins (3)<= '1';
    wait for 100 ms;
    coins(3)<='0';
    wait for 2900 ms;
    --Metemos 1,20�
    coins(1) <= '1'; --20c
    wait for 10 ms;
    coins(1) <= '0';
    coins(3) <= '1'; --1e
    wait for 10 ms;
    coins(3) <= '0';
    --Hay 1,20� -> exceso -> error se enciende 3s
    wait for 3000 ms;
    --Pulsamos reset para volver del estado de error
    reset_n <= '1';
    wait for 10 ms;
    reset_n <= '0';
    wait for 2000 ms;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;