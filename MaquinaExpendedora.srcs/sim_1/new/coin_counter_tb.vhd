library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity count_tb is
end entity;

architecture test of count_tb is

  -- Inputs
  signal clk, reset, enable : std_logic;
  signal coins	    : std_logic_vector(3 downto 0);
  
  -- Outputs
  signal enough : std_logic;
  signal error	: std_logic;

  component count is
    port (
    	clk       : in  std_logic;
        reset     : in  std_logic;
        coins	  : in std_logic_vector(3 downto 0); --[100,50,20,10]
		enable    : in std_logic;
		enough	  : out std_logic;
        error	  : out std_logic
	);
  end component;

  constant CLK_PERIOD : time := 1 ns;-- Clock period

begin
  -- Unit Under Test
  uut: count
    port map (
	  clk       => clk,
      reset     => reset,
      coins 	=> coins,
	  enough    => enough,
	  enable    => enable,
      error     => error
    );

  clkgen: process
  begin
    clk <= '1';
    wait for 0.5 * CLK_PERIOD;
    clk <= '0';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
  tester: process
  begin
    reset     <= '0';
    enable    <= '1';
    coins     <= "0000";
    
    wait until clk = '1';
    wait for 0.5 * CLK_PERIOD;
    coins(0) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(0)   <= '0';
    coins(1) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(1) <= '0';
    wait for 1 * CLK_PERIOD;
    coins(1) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(1)    <= '0';
    coins(2) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(2) <= '0';
    wait for 1 * CLK_PERIOD;
    coins(3) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(3) <= '0';
    wait for 1 * CLK_PERIOD;
    coins(1) <= '1'; --20cents
    wait for 1 * CLK_PERIOD;
    coins(1) <= '0';
    coins(3) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(3) <= '0';
    wait for 3 * CLK_PERIOD;
    coins(3) <= '1';
    wait for 1 * CLK_PERIOD;
    coins(3) <= '0';
    wait for 1 * CLK_PERIOD;
    reset <= '1';
    wait for 1 * CLK_PERIOD;
    reset <= '0';
    wait for 1* CLK_PERIOD;
    enable    <= '0';
    coins(3) <= '1';
    coins(1) <= '1';
    wait for 5* CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;